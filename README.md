# Preface

> Preface is an opinionated library designed to facilitate the
> handling of recurring functional programming idioms in
> [OCaml](https://ocaml.org). Many of the design decisions were made
> in an attempt to calibrate, as best as possible, to the OCaml
> language. Trying to get the most out of the module language. _The
> name "preface" is a nod to "Prelude"_.

## Setting up the development environment

This series of instructions explains how to build a local switch, isolated for
Preface development. It assumes you're using [OPAM](https://opam.ocaml.org/) and
as the instructions rely on the existence of the `--with-dev-setup` flag (to
install a development environment), version [`>=
2.2.0+beta1`](https://opam.ocaml.org/blog/opam-2-2-0-beta1/) is required.

```shell
opam update # update the state of opam
opam switch create . --deps-only --with-doc --with-test --with-dev-setup -y
eval $(opam env)
```
